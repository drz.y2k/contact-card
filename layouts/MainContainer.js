import React from 'react';
import ReactDOM from 'react-dom';

export default function Header({children}){

  return(<div className="container">{children}</div>)
}
