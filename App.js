import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';
import Header from './components/Header';
import Description from './components/Description';
import Footer from './components/Footer';

import MainContainer from './layouts/MainContainer';

export default function App() {

  return (
    <React.Fragment>
      <MainContainer>
        <Header />
        <Description />
        <Footer />
      </MainContainer>

    </React.Fragment>
  )
}
