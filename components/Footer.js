import React from 'react';
import ReactDOM from 'react-dom';

export default function Footer() {

  return (
    <footer>
      <img className="img-footer" src="../images/twitter_icon.png" />
      <img className="img-footer" src="../images/facebook_icon.png" />
      <img className="img-footer" src="../images/instagram_icon.png" />
      <img className="img-footer" src="../images/github_icon.png" />
    </footer>
  )
}
