import React from 'react';
import ReactDOM from 'react-dom';

export default function Description() {

  return (<div className="description--container">
    <span className='white--text name'>Laura Smith</span>
    <span className="role">Frontend Developer</span>
    <span className="website">laurasmith.website</span>
    <div className="buttons">
      <button className="emailButton">
        <img src='../images/mail_icon.png' style={{ marginRight: '9px' }} />
        Email</button>
      <button className="linkedinButton">
        <img src='../images/linkedin_icon.png' style={{ marginRight: '9px' }} />
        LinkedIn</button>
    </div>
    <div class="text-holder">
      <span className="titleInfoContent" style={{ marginTop: '42px'}}>About</span>
      <span className="textInfoContent">I am a frontend developer with a particular interest in making things simple and automating daily tasks. I try to keep up with security and best practices, and am always looking for new things to learn.</span>

      <span className="titleInfoContent">Interests</span>
      <span className="textInfoContent">Food expert. Music scholar. Reader. Internet fanatic. Bacon buff. Entrepreneur. Travel geek. Pop culture ninja. Coffee fanatic.</span>
    </div>
  </div>)
}
